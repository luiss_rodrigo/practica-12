import React from 'react';

interface VenProps {
  color: string;
  textColor: string;
  imageSrc: string;
  imageClass: string; 
  onClick: () => void;
}

const Ven: React.FC<VenProps> = ({ color, textColor, imageSrc, imageClass, onClick }) => {
  const venStyle: React.CSSProperties = {
    width: '150px',
    height: '50px',
    marginBottom: '10px',
    borderRadius: '4px',
    backgroundColor: '#ffffff',
    border: `1px solid ${color}`,
    color: textColor,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    cursor: 'pointer',
  };

  return (
    <button style={venStyle} onClick={onClick}>
      <img src={imageSrc} alt="Icon" className={imageClass} /> 
    </button>
  );
};

export default Ven;
