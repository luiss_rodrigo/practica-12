import React, { useState } from 'react';
import Checkbox from './Checkbox';
import styles from './LoginForm.module.css';

interface LoginFormProps {
  onSubmit: (email: string, password: string, rememberMe: boolean) => void;
}

const LoginForm: React.FC<LoginFormProps> = ({ onSubmit }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [rememberMe, setRememberMe] = useState(false);

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    onSubmit(email, password, rememberMe);
  };

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles.inputGroup}>
        <input
          type="email"
          id="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
          className={styles.input}
          placeholder="Email" 
        />
      </div>
      <div className={styles.inputGroup}>
      <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
          className={styles.input}
          placeholder="password" 
        />
      </div>
      <div className={styles.checkboxContainer}>
        <Checkbox label="Remember Me" checked={rememberMe} onChange={setRememberMe} />
        <p className={styles.rememberText}>Forgot Password</p> 
      </div>
      <button type="submit" className={styles.button1}>Login</button>
    </form>
  );
};

export default LoginForm;


