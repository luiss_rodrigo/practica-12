"use client";


import React from 'react';
import { useRouter } from 'next/navigation';
import LoginForm from './Components/LoginForm';
import Paragraph from './Components/Paragraph';
import Ven from './Components/Ven';
import styles from './Components/LoginForm.module.css';


const LoginPage: React.FC = () => {
    const router = useRouter();
  
    const handleLogin = (email: string, password: string, rememberMe: boolean) => {
      console.log('Logging in:', { email, password });
  
      const testEmail = 'test@example.com';
      const testPassword = '123';
  
      if (email === testEmail && password === testPassword) {
        router.push('/ruta'); 
      } else {
        alert('Invalid credentials');
      }
    };
  
    return (
      <div className={styles.container}>
        <div className={styles.container1}>
          <div className={styles.container3}>
            <img src="/Images/Icono.svg" alt="Your Icono" className={styles.icono} />
            <Paragraph text="Your Logo" className={styles.your}/>
          </div>
          <div className={styles.Container2}>
            <h1>Login</h1>
            <Paragraph text="Login to access your travelwise account" />
            <LoginForm onSubmit={handleLogin} />
            <div className={styles.dont}>
              <Paragraph text="Don’t have an account?">
                <span className={styles.signupText}>Sign up</span>
              </Paragraph>
            </div>
            <Paragraph className={styles.or} text="Or login with" />
            <div className={styles.vens}>
              <Ven color="#007bff" textColor="#fff" imageSrc="/Images/icono1.svg" imageClass="iconoclase" onClick={() => console.log('Button 1 clicked')} />
              <Ven color="#007bff" textColor="#fff" imageSrc="/Images/icono2.svg" imageClass="iconoclase" onClick={() => console.log('Button 2 clicked')} />
              <Ven color="#007bff" textColor="#fff" imageSrc="/Images/icono3.svg" imageClass="iconoclase" onClick={() => console.log('Button 3 clicked')} />
            </div>
          </div>
        </div>
        <div className={styles.imageContainer}>
          <img src="/Images/images1.svg" alt="Your Image" className={styles.image} />
        </div>
      </div>
    );
  };
  
  export default LoginPage;