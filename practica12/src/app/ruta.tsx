import React from 'react';

const RutaPage: React.FC = () => {
  return (
    <div>
      <h1>Bienvenido a la nueva ruta</h1>
      <p>Esta es la nueva página a la que se redirige después del login.</p>
    </div>
  );
};

export default RutaPage;
