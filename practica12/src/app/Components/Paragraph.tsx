import React from 'react';

interface ParagraphProps {
  text?: string;
  className?: string;
  children?: React.ReactNode;
}

const Paragraph: React.FC<ParagraphProps> = ({ text, className, children }) => {
  return (
    <p className={className}>
      {text} 
      {children} 
    </p>
  );
};

export default Paragraph;
